function isStrike(roll) {
  return roll === 10;
}

function isSpare(roll, rollPlus1) {
  return roll + rollPlus1 === 10;
}

function getRoleValue(cursor, rolls, i) {
  return cursor + i <= rolls.length - 1 ? rolls[cursor + i] : 0;
}

function isRollExist(cursor, rolls) {
  return cursor < rolls.length;
}

export default function calculateScore(rolls = []) {
  let score = 0;
  let cursor = 0;
  for (let frame = 0; frame < 10; frame += 1) {
    if (isRollExist(cursor, rolls) && isStrike(rolls[cursor])) {
      score += 10 + getRoleValue(cursor, rolls, 1) + getRoleValue(cursor, rolls, 2);
      cursor += 1;
    } else if (isRollExist(cursor, rolls) && isSpare(rolls[cursor], rolls[cursor + 1])) {
      score += 10 + getRoleValue(cursor, rolls, 2);
      cursor += 2;
    } else if (isRollExist(cursor, rolls)) {
      score += rolls[cursor] + getRoleValue(cursor, rolls, 1);
      cursor += 2;
    }
  }

  return score;
}
