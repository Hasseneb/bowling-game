import calculateScore from './scoreHelper';

test('test can score spare and three', () => {
  const rolls = [5, 5, 3, 0, 0];
  expect(calculateScore(rolls)).toBe(16);
});

test('test perfect game', () => {
  const rolls = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10];
  expect(calculateScore(rolls)).toBe(300);
});

test('test can score 9 and miss each time', () => {
  const rolls = [9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0];
  expect(calculateScore(rolls)).toBe(90);
});

test('test all 5', () => {
  const rolls = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
  expect(calculateScore(rolls)).toBe(150);
});

test('test strike followed by seven and one', () => {
  const rolls = [10, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  expect(calculateScore(rolls)).toBe(26);
});

test('test can score game of ones', () => {
  const rolls = [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0];
  expect(calculateScore(rolls)).toBe(10);
});

test('test fails to knock all pins in one frame ', () => {
  const rolls = [1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  expect(calculateScore(rolls)).toBe(6);
});

test('test can score spare followed by eight and nine', () => {
  const rolls = [6, 4, 8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  expect(calculateScore(rolls)).toBe(27);
});
