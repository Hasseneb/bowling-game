import React from 'react';
import Enzyme, { mount, shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import reducer from '../reducer';
import Game from './Game';
import addRoll from '../actions/rolls';
import { showScoreAction } from '../actions/score';
import ScoreBoard from './ScoreBoard';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('<Game /> unit test', () => {
  const getWrapper = (mockStore = createStore(reducer)) => mount(
    <Provider store={mockStore}>
      <Game />
    </Provider>,
  );

  it('component loads with initial rollNumber 0 and increments on click', () => {
    const wrapper = getWrapper();
    expect(wrapper.find('#roll-number').text()).toEqual('RollsNumber: 0');
    wrapper.find('.rolls button').first().simulate('click');
    expect(wrapper.find('#roll-number').text()).toEqual('RollsNumber: 1');
  });

  it('component loads with initial rolls () and adds a number on click', () => {
    const wrapper = getWrapper();
    expect(wrapper.find('#rolls').text()).toEqual('Rolls: ()');
    wrapper.find('.rolls button').at(6).simulate('click');
    expect(wrapper.find('#rolls').text()).toEqual('Rolls: (6)');
  });

  it('should dispatch the correct action on roll button click', () => {
    const mockStore = createStore(reducer);
    mockStore.dispatch = jest.fn();

    const wrapper = getWrapper(mockStore);
    wrapper.find('.rolls button').at(6).simulate('click');
    expect(mockStore.dispatch).toHaveBeenCalledWith(addRoll({ target: { dataset: { number: '6' } } }));
  });

  it('component loads initially without score', () => {
    const wrapper = getWrapper();
    expect(wrapper.exists('.gameScore')).toEqual(false);
  });

  it('component loads score after click', () => {
    const wrapper = getWrapper();
    wrapper.find('#calculate-button').simulate('click');
    expect(wrapper.exists('.gameScore')).toEqual(true);
  });

  it('should dispatch the correct action on click on calculate button', () => {
    const mockStore = createStore(reducer);
    mockStore.dispatch = jest.fn();

    const wrapper = getWrapper(mockStore);

    // Simulate click on calculate score and check it
    wrapper.find('#calculate-button').simulate('click');
    expect(mockStore.dispatch).toHaveBeenCalledWith(showScoreAction());
  });
});
