import React from 'react';
import RollButtons from './RollButtons';
import ScoreBoard from './ScoreBoard';
import RollBoard from './RollBoard';

function Game() {
  return (
    <div>
      <RollButtons />
      <RollBoard />
      <ScoreBoard />
    </div>
  );
}

export default Game;
