import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { calculateScore, showScoreAction } from '../actions/score';

function ScoreBoard() {
  const { score, showScore, rolls } = useSelector((state) => ({
    ...state.rolls,
    ...state.score,
  }));

  const dispatch = useDispatch();

  return (
    <div>
      { showScore && (
      <h1 className="gameScore">
        Game Score =
        <span>{score}</span>
      </h1>
      )}
      <button type="button" id="calculate-button" onClick={() => { dispatch(showScoreAction()); dispatch(calculateScore(rolls)); }}>Calculate Score</button>
    </div>
  );
}

export default ScoreBoard;
