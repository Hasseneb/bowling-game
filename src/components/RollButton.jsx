import React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import addRoll from '../actions/rolls';


function RollButton({ number }) {
  const dispatch = useDispatch();

  return (
    <button
      type="button"
      data-number={number}
      onClick={(e) => dispatch(addRoll(e))}
    >
      {number}
    </button>
  );
}

RollButton.propTypes = {
  number: PropTypes.number.isRequired,
};

export default RollButton;
