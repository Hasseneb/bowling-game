import React from 'react';
import { useSelector } from 'react-redux';

function RollBoard() {
  const { rolls, rollNumber } = useSelector((state) => ({
    ...state.rolls,
  }));
  return (
    <div className="scoreboard">
      <h4 id="rolls">
        Rolls:
        (
        { rolls.length > 0 && rolls.map((r, index) => (index > 0 ? `,${r}` : r))}
        )
      </h4>
      <h4 id="roll-number">
        RollsNumber:
        {' '}
        {rollNumber}
      </h4>
    </div>
  );
}

export default RollBoard;
