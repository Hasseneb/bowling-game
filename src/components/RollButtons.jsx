import React from 'react';
import { useSelector } from 'react-redux';
import RollButton from './RollButton';

function RollButtons() {
  const {
    rollNumber,
  } = useSelector((state) => ({
    ...state.rolls,
  }));

  return (
    <div className="rolls">
      {rollNumber < 21
          && [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((n) => (
            <RollButton
              number={n}
              key={n}
            />
          ))}
    </div>
  );
}

export default RollButtons;
