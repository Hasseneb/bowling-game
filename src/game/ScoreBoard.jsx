import React from 'react';
import Frame from './Frame';

function isSpare(rolls, i) {
  return rolls[i] + rolls[i + 1] === 10;
}

function isStrike(rolls, i) {
  return rolls[i] === 10;
}

function strikeScore(rolls, i) {
  const number1 = 10;
  const number2 = rolls[i + 1] ? rolls[i + 1] : 0;
  const number3 = rolls[i + 2] ? rolls[i + 2] : 0;
  const newScore = number1 + number2 + number3;
  return {
    number1, number2, number3, newScore,
  };
}

function spareScore(rolls, i) {
  const number1 = 10;
  const number2 = rolls[i + 2] ? rolls[i + 2] : 0;
  const newScore = number1 + number2;
  return { number2, newScore };
}

function frameScore(rolls, i) {
  const number1 = rolls[i];
  const number2 = rolls[i + 1] ? rolls[i + 1] : 0;
  let newScore = number1 + number2;
  if (newScore > 10) {
    newScore = number1;
  }
  return { number1, number2, newScore };
}

function prepareFrames(rolls, frames) {
  let score = 0;
  let i = 0;
  while (i < rolls.length) {
    if (isStrike(rolls, i)) {
      const {
        number1, number2, number3, newScore,
      } = strikeScore(rolls, i);
      score += newScore;
      frames.push(<Frame key={i} number1={number1} number2={number2} number3={number3} />);
      if (number3 !== 0) {
        i += 3;
      } else {
        i += 2;
      }
    } else if (isSpare(rolls, i)) {
      const { number2, newScore } = spareScore(rolls, i);
      score += newScore;
      frames.push(<Frame key={i} number1={10} number2={number2} number3={0} />);
      i += 3;
    } else {
      const { number1, number2, newScore } = frameScore(rolls, i);
      score += newScore;
      frames.push(<Frame key={i} number1={number1} number2={number2} number3={0} />);
      i += 2;
    }
  }
  return score;
}

function ScoreBoard({ rolls }) {
  const frames = [];
  const score = prepareFrames(rolls, frames);
  return (
    <div className="scoreboard">
      <div className="frames">
        {frames}
      </div>
      <h1>
        Game Score =
        <span style={frames.length === 10 ? { color: 'red' } : { color: 'black' }}>{score}</span>
      </h1>
    </div>
  );
}

export default ScoreBoard;
