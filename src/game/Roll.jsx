import React from 'react';


function Roll({ number, onRoll }) {
  return (
    <button
      type="button"
      data-number={number}
      onClick={(e) => onRoll(e.target.dataset.number)}
    >
      {number}
    </button>
  );
}

export default Roll;
