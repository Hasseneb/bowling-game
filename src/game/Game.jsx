import React from 'react';
import RollButtons from './RollButtons';
import ScoreBoard from './ScoreBoard';

class Game extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rollNumber: 0,
      rolls: [],
    };

    this.handleRollButtonClick = this.handleRollButtonClick.bind(this);
  }

  handleRollButtonClick(pinDown) {
    const {
      rollNumber, rolls,
    } = this.state;

    rolls[rollNumber] = +pinDown;
    this.setState({
      rollNumber: rollNumber + 1,
      rolls,
    });
  }

  render() {
    const { rolls } = this.state;
    return (
      <div>
        <RollButtons onRollButtonClick={this.handleRollButtonClick} />
        <ScoreBoard rolls={rolls} />
      </div>
    );
  }
}

export default Game;
