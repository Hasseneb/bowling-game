import React from 'react';
import Roll from './Roll';

function RollButtons({ onRollButtonClick }) {
  return (
    <div className="rolls">
      {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        .map((n) => (
          <Roll
            number={n}
            key={n}
            onRoll={(e) => onRollButtonClick(e)}
          />
        ))}
    </div>
  );
}

export default RollButtons;
