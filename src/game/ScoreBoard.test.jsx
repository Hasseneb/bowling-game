import React from 'react';
import { render } from '@testing-library/react';
import ScoreBoard from './ScoreBoard';

test('renders learn react link', () => {
  const { getByText } = render(<ScoreBoard rolls={[10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]} />);
  const linkElement = getByText(/300/i);
  expect(linkElement).toBeInTheDocument();
});
