import React from 'react';

function Frame(props) {
  let { number1, number2, number3 } = props;
  let frameScore = number1 + number2 + number3;

  if (number1 < 10 && number2 + number1 > 10) {
    frameScore = number1;
    number2 = '-';
  }

  return (
    <div className="frame">
      <span>{number1}</span>
      <span>{number2}</span>
      <p>{frameScore}</p>
    </div>
  );
}

export default Frame;
