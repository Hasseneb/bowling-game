import { combineReducers } from 'redux';
import rolls from './reducers/rolls';
import score from './reducers/score';

export default combineReducers({
  rolls,
  score,
});
