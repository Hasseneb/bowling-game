import { CALCULATE_SCORE, SHOW_SCORE } from '../constants/actionTypes';
import calculateScore from '../helpers/scoreHelper';

export default (state = {
  score: 0,
  showScore: false,
}, { type, payload }) => {
  switch (type) {
    case CALCULATE_SCORE:
      return {
        ...state,
        score: calculateScore(payload.rolls),
      };
    case SHOW_SCORE:
      return {
        ...state,
        showScore: true,
      };
    default: return state;
  }
};
