import { ADD_ROLL } from '../constants/actionTypes';

export default (state = {
  rolls: [],
  rollNumber: 0,
}, { type, payload }) => {
  const {
    rolls, rollNumber,
  } = state;

  if (type === ADD_ROLL) {
    return {
      ...state,
      rolls: [...rolls, +payload.number],
      rollNumber: rollNumber + 1,
    };
  }

  return state;
};
