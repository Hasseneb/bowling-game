import React from 'react';
import './App.css';
import Game from './game/Game';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Bowling Game</h2>
        <p>Please fill in the frames by choosing from the following numbers</p>
        <Game />
      </header>
    </div>
  );
}

export default App;
