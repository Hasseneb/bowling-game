import { ADD_ROLL } from '../constants/actionTypes';

export default function addRoll(event) {
  return {
    type: ADD_ROLL,
    payload: { number: event.target.dataset.number },
  };
}
