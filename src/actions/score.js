import { CALCULATE_SCORE, SHOW_SCORE } from '../constants/actionTypes';

export function calculateScore(rolls) {
  return {
    type: CALCULATE_SCORE,
    payload: { rolls },
  };
}

export function showScoreAction() {
  return {
    type: SHOW_SCORE,
  };
}
